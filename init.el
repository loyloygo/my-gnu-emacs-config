(menu-bar-mode 0)
;;去掉菜单栏
(tool-bar-mode 0)
;;去掉工具栏
(scroll-bar-mode 0)
;;去掉滚动条

(setq inhibit-startup-message t)
;;关闭emacs启动时的画面
(setq gnus-inhibit-startup-message t)
;;关闭gnus启动时的画面

(setq echo-keystrokes 0.1)
;; 尽快显示按键序列

(setq current-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
;;默认文本格式utf-8

(set-frame-font "Monaco-9")
;;默认字体

(add-to-list 'custom-theme-load-path "/home/loy/.emacs.d/themes/")
;;指定主题包目录
(load-theme 'zenburn t)

(set-cursor-color "#ffff00")
(set-foreground-color "white")
(set-background-color "gray10")
;;设置光标颜色、字体颜色和背景颜色

(blink-cursor-mode -1)
;;光标不要闪烁

(setq frame-title-format
      '("%S" (buffer-file-name "%f"
			       (dired-directory dired-directory "%b"))))
;;在标题栏显示当前位置

(display-time-mode 1)
;;显示时间

(font-lock-mode t)
;;开启语法高亮

(which-function-mode t)
;;在状态条上显示当前光标在哪个函数体内部

;;(setq default-directory "~/workspace/")
;;默认工作空间

;;(setq x-select-enable-clipboard t)
;;支持与linux剪切板的复制粘帖操作

(setq kill-ring-max 150)
;;设置大的kill-ring，方便以后恢复

(global-set-key (kbd "RET") 'newline-and-indent)
;;换行自动缩进

(show-paren-mode t)
(setq show-paren-style 'parentheses)
;;括号匹配时显示另一端的括号，而不是跳过去

(setq visible-bell t)
;;关闭出错提示声

(setq fill-column 120)
;;把 fill-column 设为 60. 这样的文字更好读

(setq scroll-step 1
      scroll-margin 3
      scroll-conservatively 10000)
;;滚动页面时比较舒服，不要整页的滚动

(setq auto-image-file-mode t)
;;让 Emacs 可以直接打开和显示图片。

(auto-compression-mode 1) 
;打开压缩文件时自动解压缩。

(setq backup-by-copying t)
;;备份设置方法，直接拷贝

(setq make-backup-files nil)
;;不生成临时文件

;;设置sentence-end可以识别中文标点
(setq sentence-end
      "\\([。！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")

;;=======================插件=========================
(defun my-add-subdirs-to-load-path (dir)
  "把DIR的所有子目录都加到`load-path'里面"
  (interactive)
  (let ((default-directory (concat dir "/")))
    (add-to-list 'load-path dir)
    (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
  (normal-top-level-add-subdirs-to-load-path))))
;;递归添加load目录和子目录
(defconst my-emacs-path "~/.emacs.d/" "my emacs config file path")
(defconst my-emacs-vendor-lisps-path (concat my-emacs-path "plugins/") "the vendor lisp package")
(my-add-subdirs-to-load-path my-emacs-vendor-lisps-path)


;;行号插件
(require 'linum)
(global-linum-mode 1)

;;minbuffer自动补全插件
(require 'ido)
(ido-mode t)

;; 括号自动配对插件
;;(electric-pair-mode)

;;保存会话
(require 'desktop)
;; 只使用一个desktop
(setq desktop-path '("~/.emacs.d/"))
(setq desktop-dirname "~/.emacs.d/")
(setq desktop-base-file-name "desktop")
(desktop-save-mode 1)
;; remove desktop after it's been read
(add-hook 'desktop-after-read-hook
	  '(lambda ()
	     ;; desktop-remove clears desktop-dirname
	     (setq desktop-dirname-tmp desktop-dirname)
	     (desktop-remove)
	     (setq desktop-dirname desktop-dirname-tmp)))
(defun saved-session ()
  (file-exists-p (concat desktop-dirname "/" desktop-base-file-name)))


;;IRC插件erc配置
;;编码设置
(setq erc-server-coding-system '(utf-8 . utf-8))
;;(setq erc-encoding-coding-alist '(("#linuxfire" . chinese-iso-8bit)));;GBK

;; nick name颜色
;;(require 'erc-highlight-nicknames)
(and (load-library "erc-highlight-nicknames")
     (add-to-list 'erc-modules 'highlight-nicknames)
     (erc-update-modules))

;;过滤信息
;;通过关键字匹配对相关信息进行高亮
(erc-match-mode 1)
(setq erc-keywords '("RMS" "lisp" "scheme" "Android"))
;;(setq erc-pals '("RMS" "loy"))
;;对不感兴趣的消息过滤
(setq erc-ignore-list nil)
(setq erc-hide-list
;;      '("JOIN" "PART" "QUIT" "MODE")
      )
;;保存erc的聊天记录
;; (require 'erc-log)
;; (erc-log-mode 1)
;; (setq erc-log-channel-directory "~/.emacs.d/erc/"
;;       erc-save-buffer-on-part t
;;       erc-log-file-coding-system 'utf-8
;;       erc-log-write-after-send t
;;       erc-log-write-after-insert t)
;;  (unless (file-exists-p erc-log-channels-directory)
;;   (mkdir erc-log-channels-directory t))



;;package插件管理器插件
;;(require 'package)
;;(add-to-list 'package-archives '("elpa" . "http://tromey.com/elpa/"))
;;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
;;(package-initialize)

;;;;;;;;;;;;;;;;;;;;;以下为非自带插件;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;CEDET
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (require 'cedet)
;; ;;项目管理
;; (global-ede-mode 1)
;; ;;打开代码折叠
;; (global-semantic-tag-folding-mode 1)
;; ;;折叠和打开整个buffer的所有代码
;; (define-key semantic-tag-folding-mode-map (kbd "C--") 'semantic-tag-folding-fold-all)
;; (define-key semantic-tag-folding-mode-map (kbd "C-=") 'semantic-tag-folding-show-all)

;; (semantic-load-enable-code-helpers)
;; ;;(semantic-load-enable-guady-code-helpers)
;; (global-semantic-stickyfunc-mode 0) ;;关闭将函数名显示在Buffer顶的功能
;; ;;(global-semantic-idle-completions-mode 0);;;关闭semantic的自动补全功能，因为会很慢，而且和补全插件有点冲突额
;; (global-semantic-decoration-mode 1);;启用函数名装饰一条蓝色线的功能
;; (semantic-load-enable-excessive-code-helpers)
;; (semantic-load-enable-semantic-debugging-helpers)
;; (semantic-load-enable-all-exuberent-ctags-support)
;; (setq semanticdb-project-roots (list (expand-file-name "/")))   ;配置Semantic的检索范围
;; (autoload 'senator-try-expand-semantic "senator")               ;优先调用了senator的分析结果
;; (require 'semantic-ia);;; smart complitions setup
;; (setq-mode-local c-mode semanticdb-find-default-throttle
;;                  '(project unloaded system recursive))
;; (require 'semantic-gcc);;; gcc setup
;; ;;semantic 头文件目录设置
;; (defconst cedet-user-include-dirs
;;   (list "." ".." "../include" "../inc" "../common" "../public"
;;         "../.." "../../include" "../../inc" "../../common" "../../public"))

;; (defconst cedet-sys-include-dirs
;;   (list "/usr/include"
;; 	"/usr/include/bits"
;; 	"/usr/include/glib-2.0"
;; 	"/usr/include/gnu"
;; 	"/usr/include/gtk-3.0"
;; 	"/usr/include/gtk-3.0/unix-print"
;; 	"/usr/include/gtk-3.0/gtk"
;; 	"/usr/local/include"))
;; (require 'semantic-c nil 'noerror)
;; (let ((include-dirs cedet-user-include-dirs))
;;   (when (eq system-type 'gnu/linux)
;;     (setq include-dirs (append include-dirs cedet-sys-include-dirs)))
;;   (mapc (lambda (dir)
;;           (semantic-add-system-include dir 'c++-mode)
;;           (semantic-add-system-include dir 'c-mode))
;;         include-dirs))
;; (setq semantic-c-dependency-system-include-path "/usr/include/")
;; (add-to-list 'completion-at-point-functions 'semantic-completion-at-point-function)


;;cscope
;; (require 'xcscope)
;; (require 'xcscope+);;cscope的插件扩展
;; (setq cscope-do-not-update-database t)

;; ECB
;; (setq stack-trace-on-error t)
;; (setq ecb-tip-of-the-day nil);ecb启动不显示tip
;; (require 'ecb)
;; (setq ecb-primary-secondary-mouse-buttons 'mouse-1--C-mouse-1) ;;设置可以使用鼠标点击各个窗口的东东
;; (custom-set-variables '(ecb-options-version "2.4")) ;ECB的版本, 以使启动时不检查
;;(ecb-activate)
;; ;; 各窗口间切换  
;; (global-set-key [C-left] 'windmove-left)  
;; (global-set-key [C-right] 'windmove-right)  
;; (global-set-key [C-up] 'windmove-up)  
;; (global-set-key [C-down] 'windmove-down)  


;; setting for golang
;; (require 'go-mode-load)
;; (require 'go-autocomplete)

;; setting for rust
(require 'rust-mode)
(push '("\\.rs\\'" . rust-mode) auto-mode-alist)

;; setting for smalltalk
;; (require 'smalltalk-mode)
;; (require 'gst-mode)
;; (push (cons "\\.star\\'"
;; 	    (catch 'archive-mode
;; 	      (dolist (mode-assoc auto-mode-alist 'archive-mode)
;; 		(and (string-match (car mode-assoc) "Starfile.zip")
;; 		     (functionp (cdr mode-assoc))
;; 		     (throw 'archive-mode (cdr mode-assoc))))))
;;       auto-mode-alist)
;; (push '("\\.st\\'" . smalltalk-mode) auto-mode-alist)
;; (when (boundp 'inhibit-first-line-modes-regexps)
;;   (push "\\.star\\'" inhibit-first-line-modes-regexps))
;; (autoload 'smalltalk-mode "smalltalk-mode" "" t)
;; (autoload 'gst "gst-mode" "" t)

;;代码补全插件auto-complete
(require 'auto-complete-config)
(ac-config-default)
(global-auto-complete-mode t)
;;用auto-complete-clang补全
(require 'auto-complete-clang)
;; 添加c-mode和c++-mode的hook，开启auto-complete的clang扩展  
(defun wttr/ac-cc-mode-setup ()  
  (make-local-variable 'ac-auto-start)  
  (setq ac-auto-start t) ;;auto complete using clang is CPU sensitive  
  (setq ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources)))  
(add-hook 'c-mode-hook 'wttr/ac-cc-mode-setup)  
(add-hook 'c++-mode-hook 'wttr/ac-cc-mode-setup)
(setq ac-clang-flags
      (mapcar (lambda (item)(concat "-I" item))
              (split-string
               "
/usr/include
/usr/include/bits
/usr/include/glib-2.0
/usr/include/gnu
/usr/include/gtk-3.0
/usr/include/gtk-3.0/unix-print
/usr/include/gtk-3.0/gtk
/usr/local/include
"
	       )))


;;模板补全插件yasnippet
(require 'yasnippet)
(setq yas-snippet-dirs '("/home/loy/.emacs.d/plugins/yasnippet/snippets" "/home/loy/.emacs.d/el-get/yasnippet/extras/imported"))
(yas-global-mode 1)
(require 'dropdown-list)
(setq yas-prompt-functions '(yas/dropdown-prompt
			     yas/ido-prompt
			     yas/completing-prompt))


;;设置语法检查器flymake
(autoload 'flymake-find-file-hook "flymake" "" t)
(add-hook 'find-file-hook 'flymake-find-file-hook)
(setq flymake-gui-warnings-enabled nil)
(setq flymake-log-level 0)


;;;;;;;;;;;;;;;;;;;设置C-mode;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun my-c-mode-auto-load ()
  (interactive)
  (c-set-style "linux")
  (setq c-basic-offset 8)
  (flymake-mode)
;;  (setq abbrev-mode t)
)
(add-hook 'c-mode-hook 'my-c-mode-auto-load)


;;;;;;;;;;;;;;;;;;;设置lua-mode;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

;;;;;;;;;;;;;;;;;;;设置chicken scheme;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-font-lock-mode 1)
(setq show-paren-delay 0
      show-paren-style 'parenthesis)
(show-paren-mode 1)
(setq scheme-program-name "csi")

;; ;;设置slime
;; (setq inferior-lisp-program "/usr/bin/csi")
;; (require 'slime)
;; (slime-setup '(slime-fancy slime-banner));;来自swank-chicken
;; ;;swank-chicken设置
;; (autoload 'chicken-slime "chicken-slime" "SWANK backend for Chicken" t)
;; (setq swank-chicken-path "/home/loy/.emacs.d/plugins/swank-chicken/swank-chicken.scm")
;; (add-hook 'scheme-mode-hook
;;           (lambda ()
;;             (slime-mode t)))

;;;;;;;;;;;;;;;;;;;;;设置R;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;ess插件
;; (setq ess-ask-for-ess-directory nil)
;; (setq ess-directory "/home/loy/workspace/R/")
;; (require 'ess-site)
;; (setq ess-indent-level 4)
;; (setq ess-fancy-comments nil)
;; (add-hook 'local-write-file-hooks
;; 	    (lambda ()
;; 	      (ess-nuke-trailing-whitespace)))
;; (setq ess-nuke-trailing-whitespace-p 'ask)
;; ;; r-autoyas 使得 R 和 yasnippet 一起工作
;; (require 'r-autoyas)
;; (add-hook 'ess-mode-hook 'r-autoyas-ess-activate)
;; ;;(setq r-autoyas-debug t);; Debugging
;; (setq r-autoyas-expand-package-functions-only nil);; Using functions within a namespace only


;; 括号高亮
(require 'highlight-parentheses)
;; Enables highlight-parentheses-mode on all buffers:
(define-globalized-minor-mode global-highlight-parentheses-mode
  highlight-parentheses-mode
  (lambda ()
    (highlight-parentheses-mode t)))
(global-highlight-parentheses-mode t)


;; 词典
(require 'sdcv-mode)
(global-set-key [f2] 'sdcv-search)


;;============键绑定==========

(fset 'yes-or-no-p 'y-or-n-p)
;;按 y 或空格键表示 yes，n 表示 no

(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)
;;绑定M-x到C-x\C-m

(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)
;;绑定C-w为向后删一个词，改剪切为C-x/C-k

(global-set-key "\C-h" 'backward-delete-char-untabify)
;;绑定C-h为backsspace

(global-set-key [(control return)] 'set-mark-command)
;;mark绑定为C-return

(global-set-key "\C-c m" nil)
;;去除发送e-mail的快捷键

(setq-default kill-whole-line t)
;;C-k删除整行的时候把换行符也删了

;; (global-set-key "\M-j"  'bs-cycle-previous)
;; (global-set-key "\M-k"  'bs-cycle-next)
;;快捷键切换buffer

;;(global-set-key "\C-i"  'hippie-expand)
;;智能的自动补全

;; (global-set-key [f5] 'ecb-activate)
;;f5打开ecb代码浏览功能

(global-set-key [f11] 'my-fullscreen)
(defun my-fullscreen ()
(interactive)
(x-send-client-message
nil 0 nil "_NET_WM_STATE" 32
'(2 "_NET_WM_STATE_FULLSCREEN" 0)))
;;按f11全屏
